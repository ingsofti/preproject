#! /usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.web
import tornado.escape
import tornado.auth
import tornado.websocket
import json
import psycopg2

import repository

class BaseHandler(tornado.web.RequestHandler):
  @property
  def db(self):
    if not hasattr(self, "_db"):
      self._db = psycopg2.connect("dbname=Restaurant user=postgres password=vitohe102 host=localhost port=5433")
    return self._db

class BaseRepository(BaseHandler):
  def save(self,dni,tipo,nombre,telefono,direccion):
    sql = "select insert_client(%d, '%s', '%s', '%s', '%s');" % (dni,tipo,nombre,telefono,direccion)
    cursor = self.db.cursor()
    cursor.execute(sql)
    self.db.commit()
    cursor.close()
    

class ViewClient(BaseHandler):
  def get(self):
    cursor = self.db.cursor()
    cursor.execute("select * from obtener_client();")
    result = cursor.fetchall()
    self.render("view_client.html", result=result)
  def post(self):
    eleccion = self.get_argument("eleccion",False)
    if self.get_argument("id_client",False): id_client = self.get_argument("id_client")
    if eleccion=="borrar":
      sql = "select remove_client('%s')" % (id_client)
      cursor = self.db.cursor()
      cursor.execute(sql)
      self.db.commit()
      cursor.close()
      self.redirect("/ver/client")
    elif eleccion=="modificar":
      sql = "delete from sale_head where id_sale_head='%s'" % (id_sale_head)
      cursor = self.db.cursor()
      cursor.execute(sql)
      self.db.ccommit()
      cursor.close()
      self.redirect("/ver/client")
    elif eleccion=="insertar":
      self.redirect("/insertar/client")

class InsertClient(BaseRepository):
#una simple responsabilidad
  def get(self):
    self.render("insertClient.html")
  def post(self):
    if self.get_argument("name",False): nombre = self.get_argument("name")
    else:
      self.write("Nombre requerido")

    if self.get_argument("address",False): direccion = self.get_argument("address")
    else:
      self.write("Direccion requerida")

    if self.get_argument("tipo",False): tipo = self.get_argument("tipo")
    else:
      self.write("Contrasenia requerida")

    if self.get_argument("dni",False): dni = self.get_argument("dni")
    else:
      self.write("dni requerido")

    if self.get_argument("telf",False): telefono = self.get_argument("telf")
    else:
      self.write("Cumpleanios requerido")
    dni1 = int(dni.encode("ascii"))
    self.save(dni1,tipo,nombre,telefono,direccion)
    self.redirect("/ver/client")

class IndexAdmin(BaseHandler):
  def get(self):
    self.render("viewIndex.html")
  def post(self):
    if self.get_argument("name",False): user = self.get_argument("name")
    else:
      self.write("user name requerido")
    if self.get_argument("pass",False): pass_ = self.get_argument("pass")
    else:
      self.write("password requerido")
    cursor = self.db.cursor()
    cursor.execute("select verificar_admin('vitohe102','vitohe102');")
    result = cursor.fetchall()
    if (result):
      self.redirect("/administrador")
    else:
      self.redirect("/index")


class AdministratorHandler(BaseHandler):
  def get(self):
    cursor = self.db.cursor()
    cursor.execute("select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';")
    result = cursor.fetchall()
    cursor.close()
    result = list(result)
    result.pop(-2)
    self.render("administrador.html", result=result)



class ViewWaiter(BaseHandler):
  def get(self):
    cursor = self.db.cursor()
    cursor.execute("select * from obtener_waiter();")
    result = cursor.fetchall()
    self.render("view_waiter.html", result=result)
  def post(self):
    eleccion = self.get_argument("eleccion",False)
    if self.get_argument("id_waiter",False): id_waiter = self.get_argument("id_waiter")
    if eleccion=="modificar":
        cursor = self.db.cursor()
        cursor.execute("select * from waiter where id_waiter='%s';" %(id_waiter))
        result = cursor.fetchall()
        self.render("modWaiter.html", result=result)
    elif eleccion=="modificado":
        if self.get_argument("name",False)!=False: name = self.get_argument("name")
        if self.get_argument("phone",False)!=False: phone = self.get_argument("phone")
        if self.get_argument("salario",False)!=False: salary = self.get_argument("salario")
        if self.get_argument("horario",False)!=False: horario = self.get_argument("horario")
        id_waiter = self.get_argument("id",False)
        salary1 = int(salary.encode("ascii"))
        id_waiter1 = int(id_waiter.encode("ascii"))
        sql = "select update_waiter(%d,'%s','%s',%d,'%s');" %(id_waiter1,name,phone,salary1,horario)
        cursor = self.db.cursor()
        cursor.execute(sql)
        self.db.commit()
        cursor.close()
        self.redirect("/ver/waiter")
    elif eleccion=="borrar":
        sql = "select remove_waiter('%s')" %(id_waiter)
        cursor = self.db.cursor()
        cursor.execute(sql)
        self.db.commit()
        cursor.close()
        self.redirect("/ver/waiter")
    elif eleccion=="insertar":
        self.redirect("/insertar/waiter")

class InsertWaiter(BaseHandler):
  def get(self):  
    self.render("insertWaiter.html")

  def post(self):
    if self.get_argument("name",False): name = self.get_argument("name")
    else:
      self.write("Nombre requerido")

    if self.get_argument("phone",False): phone = self.get_argument("phone")
    else:
      self.write("Telefono requerida")

    if self.get_argument("salario",False): salary = self.get_argument("salario")
    else:
      self.write("salario requerida")

    if self.get_argument("horario",False): horario = self.get_argument("horario")
    else:
      self.write("horario requerido")
    salary1 = int(salary.encode("ascii"))
    sql = "select insert_waiter('%s', '%s', %d, '%s')" % (name, phone, salary1, horario)
    cursor = self.db.cursor()
    cursor.execute(sql)
    self.db.commit()
    cursor.close()
    self.redirect("/ver/waiter")  

class ViewCategory(BaseHandler):
  def get(self):
    cursor = self.db.cursor()
    cursor.execute("select * from obtener_category();")
    result = cursor.fetchall()
    self.render("view_category.html", result=result)
  def post(self):
    eleccion = self.get_argument("eleccion",False)
    if self.get_argument("id_category",False): id_category = self.get_argument("id_category")
    if eleccion=="modificar":
        cursor = self.db.cursor()
        cursor.execute("select * from category where id_category='%s';" %(id_category))
        result = cursor.fetchall()
        self.render("modCategory.html", result=result)
    elif eleccion=="modificado":
        if self.get_argument("descripcion",False)!=False: descripcion = self.get_argument("descripcion")
        id_category = self.get_argument("id_category",False)
        id_category1 = int(id_category.encode("ascii"))
        sql = "select update_category(%d,'%s')" % (id_category1,descripcion)
        cursor = self.db.cursor()
        cursor.execute(sql)
        self.db.commit()
        cursor.close()
        self.redirect("/ver/category")
    elif eleccion=="borrar":
        sql = "select remove_category('%s')" %(id_category)
        cursor = self.db.cursor()
        cursor.execute(sql)
        self.db.commit()
        cursor.close()
        self.redirect("/ver/category")
    elif eleccion=="insertar":
        self.redirect("/insertar/category");

class InsertCategory(BaseHandler):
  def get(self):
    self.render("insertCategory.html")
  def post(self):
    if self.get_argument("name",False): name = self.get_argument("name")
    else:
      self.write("descripcion requerido")
    sql = "select insert_category('%s')" % (name)
    cursor = self.db.cursor()
    cursor.execute(sql)
    self.db.commit()
    cursor.close()
    self.redirect("/ver/category")  

class ViewDish(BaseHandler):
  def get(self):
    cursor = self.db.cursor()
    cursor.execute("select * from obtener_dish();")
    result = cursor.fetchall()
    self.render("view_dish.html", result=result)
  def post(self):
    eleccion = self.get_argument("eleccion",False)
    if self.get_argument("id_dish",False): id_dish = self.get_argument("id_dish")
    if eleccion=="modificar":
        cursor = self.db.cursor()
        cursor.execute("select * from waiter where id_waiter='%s';" %(id_waiter))
        result = cursor.fetchall()
        self.render("modWaiter.html", result=result)
    elif eleccion=="modificado":
        if self.get_argument("name",False)!=False: name = self.get_argument("name")
        if self.get_argument("phone",False)!=False: phone = self.get_argument("phone")
        if self.get_argument("salario",False)!=False: salary = self.get_argument("salario")
        if self.get_argument("horario",False)!=False: horario = self.get_argument("horario")
        id_waiter = self.get_argument("id",False)
        sql = "update waiter set name_waiter='%s', phone='%s', salary='%s', schedule='%s' where id_waiter='%s';" % (name, phone, salary,horario,id_waiter)

        cursor = self.db.cursor()
        cursor.execute(sql)
        self.db.commit()
        cursor.close()
        self.redirect("/ver/waiter")
    elif eleccion=="borrar":
        sql = "select remove_dish('%s')" %(id_dish)
        cursor = self.db.cursor()
        cursor.execute(sql)
        self.db.commit()
        cursor.close()
        self.redirect("/ver/dish")
    elif eleccion=="insertar":
       self.redirect("/insertar/dish")

class InsertDish(BaseHandler):
  def get(self):
    self.render("insertDish.html")
  def post(self):
    if self.get_argument("name",False): name = self.get_argument("name")
    else:
      self.write("descripcion requerido")
    if self.get_argument("precio",False): precio = self.get_argument("precio")
    else:
      self.write("precio requerido")
    if self.get_argument("categoria",False): categoria = self.get_argument("categoria")
    else:
      self.write("categoria requerido")
    precio1 = int(precio.encode("ascii"))
    sql = "select insert_dish('%s',%d,'%s')" % (name,precio1,categoria)
    cursor = self.db.cursor()
    cursor.execute(sql)
    self.db.commit()
    cursor.close()
    self.redirect("/ver/dish")  

class ViewSale(BaseHandler):
  def get(self):
    cursor = self.db.cursor()
    cursor.execute("select * from obtener_sale_head();")
    result = cursor.fetchall()
    self.render("view_sale.html", result=result)
  def post(self):
    eleccion = self.get_argument("eleccion",False)
    if self.get_argument("id_sale_head",False): id_sale_head = self.get_argument("id_sale_head")
    if eleccion=="detalle":
        sql = "select * from obtener_sale_body('%s');"%(id_sale_head)
        cursor = self.db.cursor()
        cursor.execute(sql)
        result = cursor.fetchall()
        self.render("view_sale_body.html", result=result)
    elif eleccion=="borrar":
        sql = "select remove_sale_head('%s')" % (id_sale_head)
        cursor = self.db.cursor()
        cursor.execute(sql)
        self.db.ccommit()
        cursor.close()
        self.redirect("/ver/sale_head")
    elif eleccion=="modificar":
        sql = "delete from sale_head where id_sale_head='%s'" % (id_sale_head)
        cursor = self.db.cursor()
        cursor.execute(sql)
        self.db.ccommit()
        cursor.close()
        self.redirect("/ver/sale_head")
    elif eleccion=="borrar_sb":
        sql = "select remove_sale_body('%s')" %(id_sale_head)
        cursor = self.db.cursor()
        cursor.execute(sql)
        self.db.ccommit()
        cursor.close()
        self.redirect("/ver/sale_head")
    elif eleccion=="modificar_sb":
        sql = "delete from sale_head where id_sale_head='%s'" % (id_sale_head)
        cursor = self.db.cursor()
        cursor.execute(sql)
        self.db.ccommit()
        cursor.close()
        self.redirect("/ver/sale_head")