#! /usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.options
from tornado.options import define, options
import handlers, os
import psycopg2

define("port",default=200,type=int)

urls = [
    (r"/index", handlers.IndexAdmin),
    (r"/administrador", handlers.AdministratorHandler),
    (r"/ver/client", handlers.ViewClient),
    (r"/ver/waiter", handlers.ViewWaiter),
    (r"/ver/category", handlers.ViewCategory),
    (r"/ver/dish", handlers.ViewDish),
    (r"/ver/sale_head", handlers.ViewSale),   
    (r"/insertar/waiter", handlers.InsertWaiter),
    (r"/insertar/client", handlers.InsertClient),
    (r"/insertar/category", handlers.InsertCategory),
    (r"/insertar/dish", handlers.InsertDish),

]

settings = dict({
  "template_path" : os.path.join(os.path.dirname(__file__),"templates"),
  "static_path" : os.path.join(os.path.dirname(__file__),"static"),
}, debug=True)

application = tornado.web.Application(urls,**settings)

def main():
  tornado.options.parse_command_line()
  server = tornado.httpserver.HTTPServer(application)
  server.listen(8080)
  tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
  main()

